//
//  ViewController.swift
//  TestContactPicker
//
//  Created by Donncha Finlay on 07/09/16.
//  Copyright © 2016 Donncha Finlay. All rights reserved.
//

import UIKit
import ContactsUI
import Photos

class ViewController: UIViewController ,CNContactPickerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //let contactPicker = CNContactPickerViewController()
        //contactPicker.displayedPropertyKeys =
          //  [CNContactEmailAddressesKey, CNContactPhoneNumbersKey]
        //self.presentViewController(contactPicker, animated: true, completion: nil)
        //contactPicker.delegate = self
        //
        //setupPhotos()
        //listAlbums()
        //testPhotoMethod()
        
    }
    override func viewDidAppear(animated: Bool) {
        testImagePicker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func testImagePicker() {
         let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        //imagePicker.sourceType = .SavedPhotosAlbum
        imagePicker.sourceType = .PhotoLibrary
        
        
        presentViewController(imagePicker, animated: true, completion: nil)
   
    }
    func testPhotoMethod() {
        // get list of all images
        let images = PHAsset.fetchAssetsWithMediaType(.Image, options: nil)
        // We have to enumerate through the fetch result as a block
        images.enumerateObjectsUsingBlock{(object: AnyObject!,
            count: Int,
            stop: UnsafeMutablePointer<ObjCBool>) in
            
            if object is PHAsset{
                print("In image list \(count)")
                let asset = object as! PHAsset
                
                self.getAssetThumbnail(asset)
                
              }
        }
        
        // Example where we select an album first
        // Set up predicate to find an album
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "title = %@", "DF1")
        let collection:PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
        //
        var albumFound:Bool
        var assetCollection = PHAssetCollection()
        
        if let first_Obj:AnyObject = collection.firstObject{
            //found the album
             assetCollection = collection.firstObject as! PHAssetCollection
             albumFound = true
        } else
        {
            albumFound = false
        }
        
        var i = collection.count
        var photoAssets = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: nil)
        let imageManager = PHCachingImageManager()
        
        
        photoAssets.enumerateObjectsUsingBlock{(object: AnyObject!,
            count: Int,
            stop: UnsafeMutablePointer<ObjCBool>) in
            
            if object is PHAsset{
                let asset = object as! PHAsset
                print("Inside  If object is PHAsset, This is number 1")
                
                let imageSize = CGSize(width: asset.pixelWidth,
                    height: asset.pixelHeight)
                
                /* For faster performance, and maybe degraded image */
                let options = PHImageRequestOptions()
                options.deliveryMode = .FastFormat
                options.synchronous = true
                imageManager.requestImageForAsset(asset,
                    targetSize: imageSize,
                    contentMode: .AspectFill,
                    options: options,
                    resultHandler: {
                        image, info in
                        var photo = image!
                        /* The image is now available to us */
                        //sendPhotos(self.photo)
                        print("enum for image, This is number 2")
                        
                        
                })
            }
        }
        
        /*PHImageCachingManager provides a single key method – startCachingImagesForAssets(...). You pass in an array of PHAssets, the request parameters and options that should match those you’re going to use later when requesting individual images.
        //print("Found Album \(collection.firstObject)")
        //print("Found \(images.firstObject)")*/
      
    }
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.defaultManager()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.synchronous = true
        manager.requestImageForAsset(asset, targetSize: CGSize(width: 100.0, height: 100.0), contentMode: .AspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
  
    func contactPicker(picker: CNContactPickerViewController, didSelectContact contact: CNContact) {
        print(contact.givenName)
    }
    
   
    }





